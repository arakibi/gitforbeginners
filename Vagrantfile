# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version '>= 2.1.5'

Vagrant.configure("2") do |config|
    config.vagrant.plugins = ["vagrant-hostsupdater", "vagrant-vbguest", "vagrant-bindfs"]

    config.vm.define "shopify", primary: true do |vm_config|
        config.vm.hostname = "shopify.local"

        config.vm.box = "debian/buster64"
        config.vm.network "private_network", ip: "192.168.60.156"

        config.ssh.forward_agent = true

        config.vm.synced_folder "./", "/vagrant", type: "virtualbox"
        config.vm.synced_folder "./src", "/vagrant-nfs", type: "nfs", create: true

        config.hostsupdater.remove_on_suspend = false

        config.bindfs.bind_folder "/vagrant-nfs", "/var/www/shopify",
            :owner => "vagrant", :group => "vagrant",
            :'create-as-user' => true,
            :'chmod-allow-x' => true, :'chown-ignore' => true, :'chgrp-ignore' => true, :'chmod-ignore' => true,
            :perms => "u=rwX:g=rwX:o=rD"

        config.vm.provider "virtualbox" do |v|
            v.name = config.vm.hostname
            v.memory = 1024
            v.cpus = 1
        end
    end

    config.vm.provision "ansible_local" do |ansible|
        ansible.pip_install_cmd = "curl https://bootstrap.pypa.io/get-pip.py | sudo python"
        ansible.become = true
        ansible.install = true
        ansible.install_mode = "pip"
        ansible.playbook = "ansible/playbook.yml"
        ansible.galaxy_role_file = "ansible/roles.yml"
        ansible.galaxy_command = "ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path} --force"
    end
end

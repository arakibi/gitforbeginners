@extends('private.base')

@section('title') Product List @endsection

@section('content')
    <table class="table">
        <thead>
            <td> Name </td>
            <td> Status </td>
            <td> Added at </td>
            <td> Action </td>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product['name'] }}</td>
                    <td>{{ $product['status'] }}</td>
                    <td>{{ $product['added_at'] }}</td>
                    <td> </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

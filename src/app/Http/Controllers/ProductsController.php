<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ProductsController
{
    public function index()
    {
        $products = [
            ['name' => 'Laptop 1', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 2', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 3', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 4', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 5', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 6', 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 7' , 'status' => 'Approved', 'added_at' => Carbon::now()],
            ['name' => 'Laptop 8', 'status' => 'Approved', 'added_at' => Carbon::now()],

        ];

        return view('private/products/index', ['products' => $products]);
    }
}

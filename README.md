## Introduction
This is the code source behind the backend service for shopify 

### Project Setup

Needed software:

`vagrant (preferably version 2.2.6)`: https://releases.hashicorp.com/vagrant/

`VirtualBox (preferably version 6.0.18)`: https://www.virtualbox.org/wiki/Download_Old_Builds

`Sequel Pro or similar(UI Interface for mysql)` https://sequelpro.com/download (Available only for mac)

The project comes with an in Ansible pre-configured VM. To set it up you need to have `vagrant` and `virtualBox`. The VM is 
run and tested on `MacOS Catalina v10.15.5`. Provisioned using `vagrant v2.2.6` and `VirtualBox v6.0.18`.

To provision the machine run `vagrant up` from the project root (the same level as `Vagrantfile`). When finished ssh to the VM by 
running `vagrant ssh` and `cd /var/www/shopify` to `composer install --dev`.
  
### SSH to the dev environment

In order to ssh to the virtual machine, run: 

```
 1 - vagrant ssh && cd /var/www/shopify
```

### Project on the browser

The project can be seen on the browser by calling the URL `https://shopify.local`. 

Notice: you may get `This is a not a secure Connection` on your browser. It's just due to the SSL Certificate. 
You can fix it by downloading the certificates and mark it as trusted. 

### Connect to the database Interface

Open Sequel Pro to set up a new connection with this configuration (don't forget to save it):

![Sequelpro configuration](sequelpro_config.png)

To test the connection is working, click on the test button. If connecting to the SSH server fails, you may need to add your 
ssh public key to the `~/.ssh/authorized_keys` in the Virtual machine (`vagrant ssh`).

### Tutorial

If you are not familiar with `vagrant` and `ansible`, I already made this tutorial that explains how to set up a dev environment

https://www.youtube.com/watch?v=PyuXr15Apxg

